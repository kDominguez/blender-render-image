import sys
import os
import subprocess
from subprocess import call

from pathlib import Path

import time

def absolute_path(file_path):
    """
    Return absolute path for the given file path

    """
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), os.path.normpath(file_path))


# --- Main ---
if __name__ == '__main__':
    debug = False

    # ///////////////////////   START     /////////////////////////////////
    print(f"\n\n[RUNNING]: {os.path.basename(sys.argv[0])} \n\n")
    

    # /////////////////////// ARGS PARSE  /////////////////////////////////

    # Check rgb validity
    try:
        color1 = [int(j)%256  for j in sys.argv[1].split(',')]
        color2 = [int(j)%256  for j in sys.argv[2].split(',')]

        if not (len(color1)== 3 and len(color2) == 3):
            raise Exception()

    except Exception as e:
        print ("Invalid rgb values ")
        sys.exit(1)

    # Set file name
    if len(sys.argv) > 3:
        fileName = sys.argv[3]
    else:
        fileName = f"render_{round(time.time()* 1000)}" # generate unique name

    # /////////////////////// PROCESS  /////////////////////////////////


    # Get processing script path
    script_path = absolute_path(os.path.join("blenderScripts", "plugins" ,"cubeTextureGradined.py"))


    # Set render output dir
    output_dir = absolute_path(os.path.join("results"))

    output_path = absolute_path(os.path.join(output_dir, fileName))

    # Set call args
    args = [
        "blender", 
        "-b", 
        "-noaudio", 
        "--python", 
        script_path, 
        '--', 
        sys.argv[1], 
        sys.argv[2],
        output_path
        ]
    

    # Force prints
    sys.stdout.flush()

    # Call processing
    return_code = subprocess.call(args)

    print(f"\n\n[EXIT]: {os.path.basename(sys.argv[0])} \n\n")

