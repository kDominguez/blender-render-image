# README #

This docker image renders a single .png frame using the blender cycles engine. The rendered image is composed of one cube with a gradient material applied to it, within an empty scene illuminated by a single point light.  
The gradient colors are passed as an rgb argument


# REQUIREMENTS #

* Working Docker enviorment

# Build image #

Open terminal and move to Dockerfile path, then run build command

* docker build -t blender-render .

# Build and start container #

Change <C:\MY\HOST\FOLDER> with the folder path on your host machine where you want to see the render results, then run command. (example command: "docker run -v C:\results:/app/results -d -t --name Container_blender-render blender-render")

* docker run -v <C:\MY\HOST\FOLDER>:/app/results -d -t --name Container_blender-render blender-render


#  Run Render - single render examples #

Run one command line at the time

### RED-GREEN ###
* docker exec -it Container_blender-render python3.9 render.py 255,0,0 0,255,0 "RED-GREEN"

### RED-BLUE ###
* docker exec -it Container_blender-render python3.9 render.py 255,0,0 0,0,255 "RED-BLUE"

### GREEN-RED ###
* docker exec -it Container_blender-render python3.9 render.py 0,255,0 255,0,0 "GREEN-RED" 

### GREEN-BLUE ###
* docker exec -it Container_blender-render python3.9 render.py 0,255,0 0,0,255 "GREEN-BLUE" 

### BLUE-GREEN ###
* docker exec -it Container_blender-render python3.9 render.py 0,0,255 0,255,0 "BLUE-GREEN" 

### BLUE-RED ###
* docker exec -it Container_blender-render python3.9 render.py 0,0,255 255,0,0 "BLUE-RED" 



#  Run Render - batch render examples #

### Copy all command lines and run all the renders in one batch  ###

docker exec -it Container_blender-render python3.9 render.py 255,0,0 0,255,0 "RED-GREEN" & ^  
docker exec -it Container_blender-render python3.9 render.py 255,0,0 0,0,255 "RED-BLUE" & ^  
docker exec -it Container_blender-render python3.9 render.py 0,255,0 255,0,0 "GREEN-RED"  & ^  
docker exec -it Container_blender-render python3.9 render.py 0,255,0 0,0,255 "GREEN-BLUE" & ^  
docker exec -it Container_blender-render python3.9 render.py 0,0,255 0,255,0 "BLUE-GREEN" & ^  
docker exec -it Container_blender-render python3.9 render.py 0,0,255 255,0,0 "BLUE-RED"

#  Result example #

![alt text](https://bitbucket.org/kDominguez/blender-render-image/raw/4366cda92ed2a5902e329a72ce86661d9e2c8057/examples/GREEN-RED.png)