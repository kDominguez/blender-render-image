
import traceback


# try catch on all the code to ensure blender closes in case of error
try:


    import sys
    import os

    import inspect
    from pathlib import Path


    #App root
    app_root = Path(inspect.getfile(inspect.currentframe())).parents[2].__str__()
    if app_root not in sys.path:
        sys.path.append(app_root)

    #Modules
    from blenderScripts.modules.factories.blender_factory import Blender 
    from blenderScripts.modules.operations import blender_operations as BlenderOps

    # --- Main ---
    if __name__ == '__main__':
        # ///////////////////////   START     /////////////////////////////////
        print(f"\n\n[RUNNING]: {os.path.basename(sys.argv[0])} \n\n")


        # /////////////////////// ARGS PARSE  /////////////////////////////////
        # Check rgb validity
        color1 = [int(j)%256  for j in sys.argv[6].split(',')] #Rgb
        color2 = [int(j)%256  for j in sys.argv[7].split(',')] #Rgb
        output_path = sys.argv[8] #Path


        if not (len(color1)== 3 and len(color2) == 3):
            raise Exception("Invalid rgb values ")

        # /////////////////////// SETUP /////////////////////////////////

        # Clean blender enviorment and set it to a known default state
        Blender.init_blender()

        # Greatly reduce background illumination
        Blender.set_background_inlumination_strenght(0.01)
 
        # Create gradient material
        mat_gradient = BlenderOps.material_create_color_gradient(color1 = color1 , color2 = color2 , matName = "gradient_Mat")

        # Create cube
        cube = BlenderOps.scene_add_cube("Cube")

        # Create light
        light = BlenderOps.scene_add_point_light("Light" , energy = 80, location=(-5,-5,5))

        # create
        camera = BlenderOps.scene_add_camera("Camera" , lens = 20 ,location=(2.5,-7.5,5))

        # Link to collection
        BlenderOps.scene_link_object_to_collection(cube)
        BlenderOps.scene_link_object_to_collection(light)
        BlenderOps.scene_link_object_to_collection(camera)

        # Assign material to cube
        BlenderOps.object_add_material(cube , mat_gradient)

        # Set scene camera (used for rendering)
        BlenderOps.scene_set_scene_camera(camera)

        # Set camera target
        BlenderOps.camera_point_to_object(camera , cube)

        # Render Image
        BlenderOps.scene_render(output_path=output_path)

        # ///////////////////////   END     /////////////////////////////////
        print(f"\n\n[EXIT]: {os.path.basename(sys.argv[0])} \n\n")
        sys.exit(0)

except Exception as e:
    red = '\033[91m'
    print(red+f"\n\n[EXCEPTION]: {os.path.basename(sys.argv[0])} FAILED =>", e, "\n\n")
    print(red+traceback.format_exc())
    sys.exit(1)