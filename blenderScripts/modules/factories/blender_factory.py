import bpy

class RenderEngine:
    blender_eevee = "BLENDER_EEVEE"
    blender_workbench = "BLENDER_WORKBENCH"
    cycles = "CYCLES"


class Blender:
    #Get pointers to blender data

    #data
    meshes = bpy.data.meshes
    lights = bpy.data.lights
    cameras = bpy.data.cameras
    materials = bpy.data.materials
    textures = bpy.data.textures
    images = bpy.data.images
    objects = bpy.data.objects

    #context
    scene = bpy.context.scene
    view_layer=bpy.context.view_layer
    collection =  bpy.context.collection

    # worlds
    world = bpy.data.worlds["World"]




    @staticmethod
    def init_blender():
        Blender.set_unit_system("METRIC")
        Blender.set_render_engine(RenderEngine().cycles)
        Blender.clear()


    @staticmethod
    def set_unit_system(unit):
        """
        Sets the blender unit system.
        """
        if Blender.scene.unit_settings.system != unit:
            Blender.scene.unit_settings.system = unit

    @staticmethod
    def set_render_engine(engine):
        """
        Sets the blender render engine
        """
        if Blender.scene.render.engine != engine:
            Blender.scene.render.engine = engine


    @staticmethod
    def render_image():
        bpy.ops.render.render(write_still=True)
    
    @staticmethod
    def delete_all_objects():
        for obj in Blender.objects:
            Blender.objects.remove(obj, do_unlink=True)


    @staticmethod
    def set_background_inlumination_strenght(strenght = 1):
        for node in  Blender.world.node_tree.nodes:
            if node.name == "Background":
                node.inputs["Strength"].default_value = strenght

    @staticmethod
    def remove_all_materials():
        for block in  Blender.materials:
            Blender.materials.remove(block)

    @staticmethod
    def remove_all_textures():
        for block in  Blender.textures:
            Blender.textures.remove(block)

    @staticmethod
    def remove_all_images():
        for block in  Blender.images:
            Blender.images.remove(block)

    @staticmethod
    def remove_all_meshes():
        for block in  Blender.meshes:
            Blender.meshes.remove(block)

    @staticmethod
    def clear():
        """
        Remove everything from the current scene

        """

        # Remove all meshes
        Blender.remove_all_meshes()

        # Remove all objects
        Blender.delete_all_objects()

        # Remove all images
        Blender.remove_all_images()

        # Remove all textures
        Blender.remove_all_textures()

        # Remove all materials
        Blender.remove_all_materials()


     

 
