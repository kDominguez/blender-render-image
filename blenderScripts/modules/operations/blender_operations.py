import sys
import os

import inspect
from pathlib import Path


#App root
app_root = Path(inspect.getfile(inspect.currentframe())).parents[2].__str__()
if app_root not in sys.path:
    sys.path.append(app_root)

#Modules
from blenderScripts.modules.factories.blender_factory import Blender 



# /////////////////////// MATERIAL OPS  /////////////////////////////////

def material_create_color_gradient(color1, color2, matName):
    """
    Create a material with a linear gradient between color A and colorB

    """

    if Blender.materials.get(matName):
        raise Exception(f"[material_color_gradient]: FAILED => material with name '{matName}' already exists")

    # set new material to variable
    mat = Blender.materials.new(name=matName)

    # Set mat to use nodes
    mat.use_nodes = True

    # Get node Tree
    nodeTree = mat.node_tree


    ###### Principled BSDF node ######
    principledBSDF = mat.node_tree.nodes['Principled BSDF'] #default node, already exits


    # Principled BSDF node - channel
    principledBSDF_channels_In = principledBSDF.inputs
    # out not needed for this use case, set by default to Material outPut on surface input channel

    # Principled BSDF node - Set default values
    principledBSDF_channels_In['Specular'].default_value = 0.0
    principledBSDF_channels_In['Metallic'].default_value = 0.2
    principledBSDF_channels_In['Roughness'].default_value = 0.8

    ###### Mix node ######
    mixNode = nodeTree.nodes.new("ShaderNodeMixRGB")

    # Set blend mode to mix
    mixNode.blend_type = "MIX"

    # Mix node - channels
    mixNode_channels_In = mixNode.inputs
    mixNode_channels_Out = mixNode.outputs

    #add alpha channel
    color1.append(1) 
    color2.append(1)

    # Mix node - Set colors 
    mixNode_channels_In["Color1"].default_value = color1
    mixNode_channels_In["Color2"].default_value = color2

    ###### Gradient Texture node ######
    gradientTexture = nodeTree.nodes.new("ShaderNodeTexGradient")

    # Gradient Texture node - channels
    gradientTexture_channels_Out = gradientTexture.outputs

    ###### Connect nodes ######

    # Generate a linear gradient factor [0;1] and feed it as input to the the Mix node
    nodeTree.links.new(gradientTexture_channels_Out["Fac"],mixNode_channels_In["Fac"]) 

    # Feed the mix color result of the Mix node to principledBSD
    nodeTree.links.new(mixNode_channels_Out["Color"],principledBSDF_channels_In['Base Color'])


    return mat


# /////////////////////// CAMERA OPS  /////////////////////////////////

def camera_point_to_object(camera, object):
    if camera:
        constraint = camera.constraints.new(type='TRACK_TO')
    else:
        raise Exception("Invalid camera ")

    if object:
            constraint.target=object
    else:
        raise Exception("Invalid object ")



# /////////////////////// OBJECT OPS  /////////////////////////////////

def object_add_material(object, material):
    """
    Add given material to given object

    """
    if material == None :
        raise Exception("Invalid material ")

    if object == None:
        raise Exception("Invalid object ")


    # Assign material to object
    object.data.materials.append(material)


# /////////////////////// SCENE OPS  /////////////////////////////////




def scene_set_scene_camera(camera):
    if camera:
        Blender.scene.camera = camera
    else:
        raise Exception("Invalid camera ")



def scene_link_object_to_collection(object):
    if object:
        Blender.collection.objects.link(object)
    else:
        raise Exception("Invalid object ")



  
def scene_add_cube(name , size=1, location=(0,0,0) ):
    """
    Add cube with given size and location to scene

    """

    # Create and link to scene
    vertices = [(size,size,-size),(size,-size,-size),(-size,-size,-size),(-size,size,-size),(size,size,size),(size,-size,size),(-size,-size,size),(-size,size,size)]
    edges = []
    faces = [(0,1,2,3),(4,5,6,7),(0,4,7,3),(0,1,5,4),(1,2,6,5),(7,6,2,3)]

    new_mesh = Blender.meshes.new(name)
    new_mesh.from_pydata(vertices, edges, faces)
    new_mesh.update()

    #make object from the mesh
    cube_object = Blender.objects.new(name, new_mesh)

    #change location
    cube_object.location = location

    return cube_object

  
def scene_add_point_light(name , energy=30, location=(0,0,0)):
    """
    Add cube with given size and location to scene

    """
   # create light data and set energy
    light_data = Blender.lights.new(name=name, type='POINT')
    light_data.energy = energy

    # create light object from light data
    light_object = Blender.objects.new(name=name, object_data=light_data)

    #change location
    light_object.location = location


    return light_object


  
def scene_add_camera(name , lens = 20 ,location=(0,0,0)):
    """
    Add cube with given size and location to scene

    """
   # create light data and set energy
    camera_data  = Blender.cameras.new(name=name)
    camera_data.lens = lens

    # create light object from light data
    camera_object = Blender.objects.new(name=name, object_data=camera_data)

    #change location
    camera_object.location = location

    return camera_object


def scene_render(output_path , file_format="PNG" ):
    Blender.scene.render.image_settings.file_format=file_format
    Blender.scene.render.filepath=output_path

    Blender.render_image()