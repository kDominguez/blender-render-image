# set base image
#https://github.com/nytimes/rd-blender-docker
FROM nytimes/blender:2.93-cpu-ubuntu18.04

# set the working directory in the container


ENV APP "/app"


WORKDIR /app

# copy files to image
COPY blenderScripts ./blenderScripts
COPY render.py .

